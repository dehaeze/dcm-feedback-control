function [data] = processMeasData(data, args)
% processMeasData -
%
% Syntax: processMeasData(data_file, lut_file, args)
%
% Inputs:
%    - data

arguments
    data
    args.fir_order (1,1) double {mustBeNumericOrLogical} = 5000
end

%% Actuator Jacobian
J_a_111 = [1,  0.14, -0.0675
           1,  0.14,  0.1525
           1, -0.14,  0.0425];

%% FIR Filter
Fs = 1e4; % Sampling Frequency [Hz]
fir_order = args.fir_order; % Filter's order
delay = fir_order/2; % Delay induced by the filter
B_fir = firls(args.fir_order, ... % Filter's order
              [0 50/(Fs/2)  0.1e3/(Fs/2) 1], ... % Frequencies [Hz]
              [1 1          0          0]); % Wanted Magnitudes

data.ddz  = 10.5e-3./(2*cos(data.bragg)) - data.dz;

%% Computation of the position of the FJ as measured by the interferometers
error = J_a_111 * [data.ddz, data.dry, data.drx]';

data.fjur_e = error(1,:)'; % [m]
data.fjuh_e = error(2,:)'; % [m]
data.fjd_e  = error(3,:)'; % [m]

%% Filtering all measured Fast Jack Position using the FIR filter
data.fjur_e_filt = filter(B_fir, 1, data.fjur_e);
data.fjuh_e_filt = filter(B_fir, 1, data.fjuh_e);
data.fjd_e_filt  = filter(B_fir, 1, data.fjd_e);

%% Compensation of the delay introduced by the FIR filter
data.fjur_e_filt(1:end-delay) = data.fjur_e_filt(delay+1:end);
data.fjuh_e_filt(1:end-delay) = data.fjuh_e_filt(delay+1:end);
data.fjd_e_filt( 1:end-delay) = data.fjd_e_filt( delay+1:end);

%% Re-sample data to have same data points in FJUR
[data.fjur_e_resampl, data.fjur_resampl] = resample(data.fjur_e_filt, data.fjur, 1/100e-9);
[data.fjuh_e_resampl, data.fjuh_resampl] = resample(data.fjuh_e_filt, data.fjuh, 1/100e-9);
[data.fjd_e_resampl,  data.fjd_resampl]  = resample(data.fjd_e_filt,  data.fjd,  1/100e-9);
