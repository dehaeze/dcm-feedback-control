function [data_struct] = extractDatData(dat_file, names, scale)
% extractDatData -
%
% Syntax: extractDatData(data_file, lut_file, args)
%
% Inputs:
%    - data_file - Where to load the .mat file
%    - lut_file - Where to save the .dat file

%% Load Data
data_array = importdata(dat_file);

%% Initialize Struct
data_struct = struct();

%% Populate Struct
for i = 1:length(names)
      data_struct.(names{i}) = scale(i)*data_array(:,i);
end

%% Add Time
data_struct.time = 1e-4*[1:1:length(data_struct.(names{1}))];
